json.extract! student, :id, :name, :surname, :birth_date, :created_at, :updated_at
json.url student_url(student, format: :json)
